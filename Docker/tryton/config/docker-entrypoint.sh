#!/bin/bash
# Waiting for database
while ! nc -z db 5432; do sleep 2; done

export PATH=~/.local/bin:$PATH

# Download trytond and modules
pip install --pre --find-links https://trydevpi.tryton.org/ --user -r ~/config/requirements.txt
pip install --pre --find-links https://trydevpi.tryton.org/ --user -r ~/config/trytond_modules.txt

# Install active modules
trytond-admin -c /home/tryton/config/trytond.conf -d $POSTGRES_DB --all $TRYTOND_LANG

# Executes trytond
if [ $TRYTOND_DEV -eq 1 ]; then
    echo "Starting in development mode"
    trytond -c ~/config/trytond.conf -d $POSTGRES_DB -v --dev --logconf ~/config/logging.conf
else
    echo "Starting in production mode"
    trytond -c ~/config/trytond.conf -d $POSTGRES_DB --logconf ~/config/logging.conf
fi

# Delete trytonpassfile
rm $TRYTONPASSFILE