# -*- coding: utf-8 -*-

from proteus import config, Model
from pymongo import MongoClient


families = {}
categories = {}
subcategories = {}
products = {}
client = None
categories_cursor = None


def proteus_connect():
    path = "/opt/tryton/tryton.conf"
    config.set_trytond(database='tryton', user='admin', config_file=path)


def mongodb_connect():
    client = MongoClient('mongodb://lc_db:27017/')
    return client.lemoncash.categorias


def mongo_categories(column="familia", filter={}, parent=None):
    families_mongodb = categories_cursor.distinct(column, filter)
    parent_obj = None
    if parent is not None:
        Category = Model.get('product.category')
        parent_obj = Category(id=parent)

    old = None
    for item in families_mongodb:
        if old != item:
            add_category(item, parent_obj, column)
            old = item


def add_category(name, parent, column):
    Category = Model.get('product.category')
    category = Category()
    category.name = name
    if parent is not None:
        category.parent = parent
        category.account_parent = True
        category.taxes_parent = True
        category.tariff_codes_parent = True
    else:
        category.accounting = True
        category.customs = True

    category.save()

    if column == 'familia':
        families[name] = category.id
    elif column == 'categoria':
        categories[name] = category.id
    elif column == 'subcategoria':
        subcategories[name] = category.id
    elif column == 'nombre':
        products[name] = category.id


if __name__ == "__main__":
    proteus_connect()
    categories_cursor = mongodb_connect()
    mongo_categories()
    for k, v in families.iteritems():
        mongo_categories(
            column="categoria",
            filter={'familia': k},
            parent=v)

    for k, v in categories.iteritems():
        mongo_categories(
            column="subcategoria",
            filter={'categoria': k},
            parent=v)

    for k, v in subcategories.iteritems():
        mongo_categories(
            column="nombre",
            filter={'subcategoria': k},
            parent=v)
